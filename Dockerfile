FROM gitlab-registry.cern.ch/linuxsupport/cs8-base

ARG JAVA_PACKAGE=java-11-openjdk-headless

ENV JAVA_HOME /usr/lib/jvm/jre
ENV LANG C.UTF-8

# Works for JDK and JRE installations
ENV PATH $PATH:$JAVA_HOME/jre/bin:$JAVA_HOME/bin

RUN yum update -y && \
    dnf install -y  epel-release CERN-CA-certs ${JAVA_PACKAGE} rsync sshpass jq && \
    yum clean all
